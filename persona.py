# -*- coding: utf-8 -*-
class Persona:

    def atributos(self, nombre, apellido, edad):
        self.nombre = nombre
        self.apellido = apellido
        self.edad = edad

    def mostrar(self):
        print "Mi nombre completo es {} {} y tengo {} años".format(self.nombre, self.apellido, self.edad)

obj = Persona()
obj.atributos("Marcos", "Osorio", 16)
obj.mostrar()
